package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"

	oidc "github.com/coreos/go-oidc"
	"github.com/joho/godotenv"
	"golang.org/x/net/context"
)

var appNonce string

func main() {
	// Load env from .env
	godotenv.Load()
	// Other variables
	appNonce = os.Getenv("OIDC_NONCE")
	state := os.Getenv("OIDC_STATE")
	listenAt := os.Getenv("OIDC_LISTEN_AT")
	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, os.Getenv("OIDC_ISSUER"))
	if err != nil {
		log.Fatal(err)
	}

	oidcConfig := &oidc.Config{
		ClientID: os.Getenv("OIDC_CLIENT_ID"),
	}
	// Use the nonce source to create a custom ID token verifier
	nonceEnabledVerifier := provider.Verifier(oidcConfig)

	config := oauth2.Config{
		ClientID:     os.Getenv("OIDC_CLIENT_ID"),
		ClientSecret: os.Getenv("OIDC_CLIENT_SERET"),
		Endpoint:     provider.Endpoint(),
		RedirectURL:  fmt.Sprintf("http://%s/authorization-code/callback", listenAt), //"http://localhost:3030/authorization-code/callback",
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email", oidc.ScopeOfflineAccess},
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Handling path '/'. Redirecting to authcodeurl")
		http.Redirect(w, r, config.AuthCodeURL(state, oidc.Nonce(appNonce)), http.StatusFound)
	})

	http.HandleFunc("/authorization-code/callback", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Handling authcode callback.")
		if r.URL.Query().Get("state") != state {
			log.Printf("Wanted state=%s. got state=%s", state, r.URL.Query().Get("state"))
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}
		oauth2Token, err := config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			log.Printf("Error at exchange: %v", err)
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		refreshToken, ok := oauth2Token.Extra("refresh_token").(string)
		if !ok {
			log.Println("refresh_token didnt work")
			_ = refreshToken
		}
		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			log.Println("Error at oauth2Token.Extra. Maybe no id_token field in oauth2 token")
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}
		idToken, err := nonceEnabledVerifier.Verify(ctx, rawIDToken)
		if err != nil {
			log.Printf("Error verifying idtoken: %v", err)
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
		}
		if idToken.Nonce != appNonce {
			log.Printf("Invalid ID token nonce: %s", idToken.Nonce)
			http.Error(w, "Invalid ID Token Nonce", http.StatusInternalServerError)
		}
		oauth2Token.AccessToken = "*REDACTED*"

		resp := struct {
			OAuth2Token   *oauth2.Token
			IDTokenClaims *json.RawMessage // ID Token payload is just json
		}{oauth2Token, new(json.RawMessage)}

		if err := idToken.Claims(&resp.IDTokenClaims); err != nil {
			log.Printf("Error unload idtoken claim into json: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		data, err := json.MarshalIndent(resp, "", "    ")
		if err != nil {
			log.Printf("Error marshalling resp struct: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(data)
	})

	log.Printf("Listening on http://%s", listenAt)
	log.Fatal(http.ListenAndServe(listenAt, nil))
}
